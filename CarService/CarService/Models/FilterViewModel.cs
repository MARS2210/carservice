﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CarService.HelpModels;

namespace CarService.Models
{
    public class FilterViewModel
    {
        public IEnumerable<CarIndexViewModel> Cars { get; set; }

        public List<CheckModel> Marks { get; set; }

        public string[] MarkList { get; set; }

        [MinLength(3, ErrorMessage = "Введите не меньше 3 букв")]
        [Display(Name = "Поиск по модели")]
        public string SearchString { get; set; }

        [Range(1, 10000,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        [Display(Name = "Цена от, грн")]
        public decimal? MinPrice { get; set; }

        [Range(1, 1000,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        [Display(Name = "Цена до, грн")]
        public decimal? MaxPrice { get; set; }

        public bool License { get; set; }

    }
}