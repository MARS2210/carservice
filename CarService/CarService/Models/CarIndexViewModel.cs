﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CarService.DAL.Entities;

namespace CarService.Models
{
    public class CarIndexViewModel
    {
        public int CarId { get; set; }

        [Required]
        [Display(Name = "Номер")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Марка")]
        public string Mark { get; set; }

        [Required]
        [Display(Name = "Модель")]
        public string CarModel { get; set; }

        [Required]
        [Display(Name = "Тип кузова")]
        public string Type { get; set; }

        [Required]
        public string Color { get; set; }

        [Display(Name = "Объем двигателя, л")]
        public float EngineVol { get; set; }

        [Display(Name = "Цена за сутки, грн")]
        public decimal PricePerDay { get; set; }

        [Display(Name = "Год выпуска")]
        public int ReleaseYear { get; set; }

        [Display(Name = "Пробег")]
        public int MileAge { get; set; }

        [Display(Name = "Количество")]
        public int UnitsInStock { get; set; }

        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo { get; set; }

        public byte[] ImageBytes { get; set; }

        public string ImgMimeType { get; set; }

        public bool Check { get; set; }
        public int ShipperId { get; set; }
     
        public ShipperViewModel Shipper { get; set; }
    }
}