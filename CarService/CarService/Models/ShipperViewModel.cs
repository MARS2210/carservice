﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarService.Models
{
    public class ShipperViewModel
    {
        public int ShipperId { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string ShipperName { get; set; }

        [Display(Name = "Страна")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string Country { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string Address { get; set; }

        [Display(Name = "Контактное лицо")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string ContactPerson { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"\(\d\d\d\) \d\d\d-\d\d-\d\d",
            ErrorMessage = "Номер телефона должен иметь формат: (095) 111-11-11")]
        public string Phone { get; set; }

        //public ICollection<CarViewModel> Cars { get; set; }
    }
}