﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CarService.DAL.Entities;

namespace CarService.Models
{
    public class CarViewModel
    {
        public int CarId { get; set; }
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        [Display(Name = "Номер")]
        [MinLength(8, ErrorMessage = "Длина номера карты должна быть 8 символов")]
        [MaxLength(8, ErrorMessage = "Длина номера карты не должна быть 8 символов")]
        [RegularExpression(@"[A-Z]{2}[0-9]{4}[A-Z]{2}", ErrorMessage = "Номер должен быть в формате АКТ1234ВК")]
        public string Number { get; set; }

        [Display(Name = "Марка")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string Mark { get; set; }

  
        [Display(Name = "Модель")]
        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        public string CarModel { get; set; }

        [Required(ErrorMessage = "Значение поля должно присутствовать!")]
        [Display(Name = "Тип кузова")]
        public string Type { get; set; }

        //[Required(ErrorMessage = "Значение поля должно присутствовать!")]
        //public string Color { get; set; }

        [Display(Name = "Объем двигателя, л")]
        [Range(1, 3,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public float EngineVol { get; set; }

        [Display(Name = "Цена за сутки, грн")]
        [Range(typeof(decimal), "100,0", "1000,0", ErrorMessage = "Сумма должна быть больше 100 грн")]      
        //[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.Currency)]
        //[RegularExpression(@"^\d+(.\d\d)?$", ErrorMessage = "Сумма должна иметь формат 100.01")]
        public decimal PricePerDay { get; set; }

        [Display(Name = "Год выпуска")]
        [Range(2000, 2017,
        ErrorMessage = "Значение {0} должно быть между {1} и текущим годом.")]
        public int ReleaseYear { get; set; }

        [Display(Name = "Пробег")]
        [Range(1, 10000,ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public int MileAge { get; set; }

        [Display(Name = "Количество")]
        [Range(1, 100, ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public int UnitsInStock { get; set; }

        [Display(Name = "Дополнительная информация")]
        public string AdditionalInfo { get; set; }
   
        public byte[] ImageBytes { get; set; }

        public string ImgMimeType { get; set; }

        [Display(Name = "Поставщик")]
        public int ShipperId { get; set; }

        public IEnumerable<ShipperViewModel> Shippers { get; set; }

        //public ICollection<OrderViewModel> Orders { get; set; }

        public ShipperViewModel Shipper { get; set; }
    }
}