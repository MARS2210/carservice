﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using CarService.BLL.HelpModels;

namespace CarService.Models
{
    public class DateViewModel
    {
        //public DateViewModel()
        //{
        //    StartDate = DateTime.UtcNow.AddYears(-2);
        //    EndDate = DateTime.UtcNow.AddDays(+1);
        //}

        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //public DateTime StartDate { get; set; }


        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //public DateTime EndDate { get; set; }

        [Display(Name = "Год")]
        [Range(2000, 2017, ErrorMessage = "Значение {0} должно быть между {1} и текущим годом")]
        public int StartYear { get; set; }

        public IEnumerable<GroupDate> Data { get; set; }

        
    }
}