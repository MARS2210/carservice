﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CarService.BLL.DTO;
using CarService.DAL.Entities;
using CarService.DAL.Enums;

namespace CarService.Models
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            StartDate = DateTime.UtcNow;
            EndDate = DateTime.UtcNow.AddDays(+1);
        }
        public int OrderId { get; set; }

        [Display(Name = "Дата заказа")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата начала аренды")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Дата окончания аренды")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата возврата")]
        public DateTime? ReturnDate { get; set; }

        [Display(Name = "Скидка, %")]
        [Range(0, 100,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public float? Discount { get; set; }

        [Display(Name = "Штраф, %")]
        [Range(0, 100,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public float? Penalty { get; set; }

        [Display(Name = "Количество")]
        [Range(1, 10,
        ErrorMessage = "Значение {0} должно быть между {1} и {2}.")]
        public int Quantity { get; set; }

        public int? OldQuantity { get; set; }

        [Display(Name = "Статус")]
        public OrderStatus PaymentState { get; set; }

        [Display(Name = "Сумма")]
        public decimal Sum { get; set; }

        public int CarId { get; set; }

        public bool Check { get; set; }
        public string ApplicationUserId { get; set; }

        public ApplicationUser User { get; set; }

        public CarIndexViewModel Car { get; set; }
    }
}