﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.Interfaces;
using CarService.DAL.Entities;
using CarService.Models;

namespace CarService.Controllers
{
    public class ShippersController : Controller
    {
        private IShipperService _shipperService;

        public ShippersController( IShipperService shipperService)
        {            
            _shipperService = shipperService;
        }

        // GET: Cars
        public ActionResult Index()
        {
            var cars = _shipperService.GetAllShippers();
            var model = Mapper.Map<IEnumerable<ShipperDto>, IEnumerable<ShipperViewModel>>(cars);
            return View(model.ToList());
        }

        // GET: Cars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shipper = _shipperService.GetShipperById((int)id);
            var model = Mapper.Map<ShipperDto, ShipperViewModel>(shipper);
            if (shipper == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Cars/Create
        public ActionResult Create()
        {
            var model = new ShipperViewModel();          
            return View(model);
        }

        // POST: Cars/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ShipperViewModel model)
        {
            if (ModelState.IsValid)
            {
                var shipper =
                   Mapper.Map<ShipperViewModel,ShipperDto>(model);
                _shipperService.AddShipper(shipper);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Cars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shipper = _shipperService.GetShipperById((int)id);
            var model = Mapper.Map<ShipperDto, ShipperViewModel>(shipper);
            if (shipper == null)
            {
                return HttpNotFound();
            }
           
            return View(model);
        }

        // POST: Cars/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ShipperViewModel model)
        {
            if (ModelState.IsValid)
            {
                var shipper =
                   Mapper.Map<ShipperViewModel, ShipperDto>(model);
                _shipperService.EditShipper(shipper);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Cars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shipper = _shipperService.GetShipperById((int)id);
            var model = Mapper.Map<ShipperDto, ShipperViewModel>(shipper);
            if (shipper == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _shipperService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}
