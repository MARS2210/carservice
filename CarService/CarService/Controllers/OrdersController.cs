﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.HelpModels;
using CarService.BLL.Interfaces;
using CarService.DAL.Entities;
using CarService.Models;
using Microsoft.AspNet.Identity;

namespace CarService.Controllers
{
    public class OrdersController : Controller
    {
        private IOrderService _orderService;
        private ICarService _carService;

        public OrdersController(IOrderService shipperService, ICarService carService)
        {
            _orderService = shipperService;
            _carService = carService;
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        public ActionResult Buy(int id)
        {                                 
            var orderDetailsViewModel = new OrderViewModel();
            orderDetailsViewModel.CarId = id;
            orderDetailsViewModel.ApplicationUserId = User.Identity.GetUserId();
            return View(orderDetailsViewModel);
        }
        [HttpPost]
        [Authorize(Roles = "User")]
        public ActionResult Buy(OrderViewModel orderDetailsViewModel)
        {
            if (ModelState.IsValid)
            {
                var orderDetail = Mapper.Map<OrderViewModel, OrderDto>(orderDetailsViewModel);
                var unisInStock = _carService.CheckUnitsInStock(orderDetailsViewModel.CarId);             
                if (unisInStock >= orderDetailsViewModel.Quantity)
                {
                    if (_orderService.CountWithoutPenalty(User.Identity.GetUserId()) > 2 && _orderService.CountWithPenalty(User.Identity.GetUserId()) == 0)
                    {
                        orderDetail.Discount = 5;
                    }
                    if (_orderService.CountWithoutPenalty(User.Identity.GetUserId()) > 2 && _orderService.CountWithPenalty(User.Identity.GetUserId()) == 0)
                    {
                        orderDetail.Discount = 20;
                    }
                    _orderService.AddOrder(orderDetail);
                    Response.StatusCode = (int)HttpStatusCode.Created;                 
                    var messageBuy = String.Format("Вы забронировали {0} машину(ы) {1} ", orderDetail.Quantity, orderDetailsViewModel.CarId);
                    TempData["Buy"] = messageBuy;
                    return RedirectToAction("Index", "Cars");
                }
                TempData["Message"] = "Недостаточно машин";
                return View();
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return View(orderDetailsViewModel);
        }
        // GET: Cars
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var orders = _orderService.GetAllOrders();
            var model = Mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderViewModel>>(orders);
            return View(model.ToList());
        }
        [Authorize(Roles = "User")]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult IndexForUser()
        {
            var orders = _orderService.GetAllOrdersForUser(User.Identity.GetUserId());
            var model = Mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderViewModel>>(orders);
            return View("Index", model.ToList());
        }
        // GET: Cars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var order = _orderService.GetOrderById((int)id);
            var model = Mapper.Map<OrderDto, OrderViewModel>(order);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Cars/Create    
        public ActionResult Create()
        {
            var model = new OrderViewModel();
            return View(model);
        }

        // POST: Cars/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]      
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var order =
                   Mapper.Map<OrderViewModel, OrderDto>(model);
                order.CarId = 8;
                _orderService.AddOrder(order);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Cars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shipper = _orderService.GetOrderById((int)id);
            var model = Mapper.Map<OrderDto, OrderViewModel>(shipper);
            model.OldQuantity = model.Quantity;
            if (shipper == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }
        public ActionResult EditForUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shipper = _orderService.GetOrderById((int)id);
            var model = Mapper.Map<OrderDto, OrderViewModel>(shipper);
            model.OldQuantity = model.Quantity;
            if (shipper == null)
            {
                return HttpNotFound();
            }

            return View("EditForUser", model);
        }
        // POST: Cars/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OrderViewModel model)
        {
            var order = Mapper.Map<OrderViewModel, OrderDto>(model);
            if (ModelState.IsValid)
            {
                var unisInStock = _carService.CheckUnitsInStock(model.CarId);
                if (unisInStock + model.OldQuantity >= model.Quantity)
                {
                    _orderService.EditOrder(order, model.OldQuantity);
                    Response.StatusCode = (int)HttpStatusCode.Created;
                    var messageBuy = String.Format("Вы забронировали {0} машину(ы) {1} ", order.Quantity, order.CarId);
                    TempData["Buy"] = messageBuy;
                    if (User.IsInRole("Admin"))
                    {
                        return RedirectToAction("Index", "Orders");
                    }
                    return RedirectToAction("IndexForUser", "Orders", new {id = User.Identity.GetUserId()});
                }
                TempData["Message"] = "Недостаточно машин";
                if (User.IsInRole("Admin"))
                {
                    return View();
                }               
                return View("EditForUser");
                
            }
            if (User.IsInRole("Admin"))
            {
                return View(model);
            }
            return View("EditForUser",model);
        }

        // GET: Cars/Delete/5
      

        // POST: Cars/Delete/5
        [ActionName("Delete")]       
        public ActionResult DeleteConfirmed(int id)
        {
            _orderService.Remove(id);
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Orders");
            }
            return RedirectToAction("IndexForUser", "Orders", new { id = User.Identity.GetUserId() });
        }

        [HttpGet]
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]       
        public ActionResult Pay(int id)
        {
            var order = _orderService.GetOrderById(id);
            _orderService.ChangeOrderToConfirmed(id);
            var orderViewModel = Mapper.Map<OrderDto, OrderViewModel>(order);
          
            return PartialView("Bill", orderViewModel);
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult GeneratePDF(int id)
        {
           
            return new Rotativa.ActionAsPdf("Pay", new { id = id });
        }

        public ActionResult GeneratePDFAnaliz(int id)
        {
           
            return new Rotativa.ActionAsPdf("PdfByDate", new { start = id });
        }

        public JsonResult GetCostByDate(string start)
        {
            var startYear = int.Parse(start.Substring(0,4));
         
            var ordersByYear = _orderService.GetOrdersBetweenYears(startYear).ToList();
            var orders = ordersByYear.GroupBy(x => x.OrderDate.ToMonthName()).ToList();
            var queryCostDate = orders.Select(x => new GroupDate()
            {
                Date = x.Key,
                Cost = x.Sum(y => y.Sum),
                Count = x.Sum(y => y.Quantity)

            }).ToList();
         
            return Json(new { Countries = queryCostDate }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ReportByDate()
        {
            var model = new DateViewModel();
            var orders = _orderService.GetOrdersBetweenYears(2016).GroupBy(x => x.OrderDate.ToMonthName()).ToList();
            var queryCostDate = orders.Select(x => new GroupDate()
            {
                Date = x.Key,
                Cost = x.Sum(y => y.Sum),
                Count = x.Sum(y => y.Quantity)

            }).ToList();
            model.Data = queryCostDate;
            model.StartYear = 2016;            
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchByDate(DateViewModel model)
        {
           
            var ordersByYear = _orderService.GetOrdersBetweenYears(model.StartYear).ToList();
            var orders = ordersByYear.GroupBy(x => x.OrderDate.ToMonthName()).ToList();
            var queryCostDate = orders.Select(x => new GroupDate()
            {
                Date = x.Key,
                Cost = x.Sum(y => y.Sum),
                Count = x.Sum(y => y.Quantity)

            }).ToList();
            model.Data = queryCostDate;
            return View("ReportByDate", model);
        }

        [HttpGet]
        public ActionResult PdfByDate(int start)
        {
          
            var model = new DateViewModel();
            var ordersByYear = _orderService.GetOrdersBetweenYears(start).ToList();
            var orders = ordersByYear.GroupBy(x => x.OrderDate.ToMonthName()).ToList();
            var queryCostDate = orders.Select(x => new GroupDate()
            {
                Date = x.Key,
                Cost = x.Sum(y => y.Sum),
                Count = x.Sum(y => y.Quantity)

            }).ToList();
            model.Data = queryCostDate;
            model.StartYear = start;
            return View("Report", model);
        }

    }
}
