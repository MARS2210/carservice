﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.Interfaces;
using CarService.HelpModels;
using CarService.Models;
using Microsoft.AspNet.Identity;


namespace CarService.Controllers
{
    public class CarsController : Controller
    {
        private ICarService _carService;
        private IShipperService _shipperService;

        public CarsController(ICarService service, IShipperService shipperService)
        {
            _carService = service;
            _shipperService = shipperService;
        }

 
        public ActionResult Index()
        {
            var cars = _carService.GetAllCars();
            var model = Mapper.Map<IEnumerable<CarDto>, IEnumerable<CarIndexViewModel>>(cars);
            var modelFilter = new FilterViewModel();
            if (User.IsInRole("User"))
            {
                foreach (var carIndexViewModel in model)
                {
                    if (_carService.Check(User.Identity.GetUserId(), carIndexViewModel.CarId))
                    {
                        carIndexViewModel.Check = true;
                    }
                    if (_carService.CheckLicense(User.Identity.GetUserId()))
                    {
                        modelFilter.License = true;
                    }
                }
            }
         
            modelFilter.Cars = model;
            var listMarks = new List<CheckModel>();            
            ConfigureCheckBoxs(listMarks);
            modelFilter.Marks = listMarks;          
            return View(modelFilter);
        }

        [HttpPost]
        public ActionResult Games(FilterViewModel filters)
        {
            var listMarks = new List<CheckModel>();
         
            SetCheckBoxs(listMarks, filters.MarkList);
            filters.Marks = listMarks;
           
            var filterDto = Mapper.Map<FilterViewModel, FilterDto>(filters);
            filters.Cars =
                  Mapper.Map<List<CarIndexViewModel>>(
                      _carService.GetFiltered(
                          filterDto));
            if (User.IsInRole("User"))
            {
                foreach (var carIndexViewModel in filters.Cars)
                {
                    if (_carService.Check(User.Identity.GetUserId(), carIndexViewModel.CarId))
                    {
                        carIndexViewModel.Check = true;
                    }
                }
            }
            return PartialView("Filtered", filters);
        }

        private void SetCheckBoxs(List<CheckModel> listMarks, string[] marks)
        {
            var marksList = new string[] { "Mazda", "BMW", "Lexus", "Nissan", "CHEVROLET", "HYUNDAI", "HONDA", "MITSUBISHI", "MERCEDES" };
            Array.Sort(marksList);
            foreach (var item in marksList)
            { 
                if (marks != null && marks.Contains(item))
                {
                    listMarks.Add(new CheckModel() {Name =item, Checked = true});
                }
                else
                {
                    listMarks.Add(new CheckModel() {Name = item, Checked = false});
                }
            }
        }

        // GET: Cars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var car = _carService.GetCarById((int)id);
            var model = Mapper.Map<CarDto, CarIndexViewModel>(car);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Cars/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var model = new CarViewModel();
            var shippers = _shipperService.GetAllShippers();
            var shippersForModel = Mapper.Map<IEnumerable<ShipperDto>, IEnumerable<ShipperViewModel>>(shippers);
            model.Shippers = shippersForModel;
            return View(model);
        }

        // POST: Cars/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(CarViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var car =
                   Mapper.Map<CarViewModel, CarDto>(model);
                if (file != null && file.ContentType.StartsWith("image"))
                {
                    _carService.DownloadImage(car, file.ContentType, file.ContentLength);
                    file.InputStream.Read(car.ImageBytes, 0, file.ContentLength);
                }
                _carService.AddGame(car);
                return RedirectToAction("Index");
            }
            var shippers = _shipperService.GetAllShippers();
            var shippersForModel = Mapper.Map<IEnumerable<ShipperDto>, IEnumerable<ShipperViewModel>>(shippers);
            model.Shippers = shippersForModel;
            return View(model);
        }

        // GET: Cars/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var car = _carService.GetCarById((int)id);
            var model = Mapper.Map<CarDto, CarViewModel>(car);
            if (car == null)
            {
                return HttpNotFound();
            }
            var shippers = _shipperService.GetAllShippers();
            var shippersForModel = Mapper.Map<IEnumerable<ShipperDto>, IEnumerable<ShipperViewModel>>(shippers);
            model.Shippers = shippersForModel;
            return View(model);
        }

        // POST: Cars/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CarViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var car =
                  Mapper.Map<CarViewModel, CarDto>(model);
                if (file != null && file.ContentType.StartsWith("image"))
                {
                    _carService.DownloadImage(car, file.ContentType, file.ContentLength);
                    file.InputStream.Read(car.ImageBytes, 0, file.ContentLength);
                }
                _carService.EditCar(car);
                return RedirectToAction("Index");
            }
            var shippers = _shipperService.GetAllShippers();
            var shippersForModel = Mapper.Map<IEnumerable<ShipperDto>, IEnumerable<ShipperViewModel>>(shippers);
            model.Shippers = shippersForModel;
            return View(model);
        }

       
        // POST: Cars/Delete/5
        [ActionName("Delete")]        
        public ActionResult DeleteConfirmed(int id)
        {
           _carService.Remove(id);
            return RedirectToAction("Index");
        }

        public void ConfigureCheckBoxs(List<CheckModel> listMarks)
        {
            var marks = new string[] {"Mazda", "BMW", "Lexus", "Nissan", "CHEVROLET", "HYUNDAI", "HONDA", "MITSUBISHI", "MERCEDES" };
            Array.Sort(marks);
            foreach (var item in marks)
            {            
                listMarks.Add(new CheckModel() { Name = item, Checked = false });
            }         
        }

        public FileResult GameImage(int id)
        {
            var imageData = _carService.GetImage(id);
            return File(imageData.Item1, imageData.Item2);
        }
    }
}
