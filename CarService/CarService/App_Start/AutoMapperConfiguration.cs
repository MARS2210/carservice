﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using CarService.BLL.Infrastructure;

namespace CarService.App_Start
{
 
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DtoToViewModelMappingProfile>();
                x.AddProfile<DaltoDtoModelMappingProfile>();            
            });
        }
    }
}