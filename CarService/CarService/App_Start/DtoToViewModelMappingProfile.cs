﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.Models;

namespace CarService.App_Start
{
    public class DtoToViewModelMappingProfile : Profile
    {

        public override string ProfileName
        {
            get { return "DtoToViewModelMappingProfile"; }
        }

        protected override void Configure()
        {

            CreateMap<CarDto, CarViewModel>().ReverseMap();

            CreateMap<OrderDto, OrderViewModel>().ReverseMap();

            CreateMap<ShipperDto, ShipperViewModel>().ReverseMap();

            CreateMap<CarDto, CarIndexViewModel>().ReverseMap();

            CreateMap<FilterDto, FilterViewModel>().ReverseMap();
        }
    }
}