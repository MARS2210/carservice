﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ApplicationServices;
using System.Web.Mvc;
using CarService.BLL.Interfaces;
using CarService.BLL.Services;
using CarService.DAL.Repository.Abstract;
using CarService.DAL.Repository.Concrete;
using Microsoft.Owin.Logging;
using Ninject;
using WebGrease;

namespace CarService.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            _kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            _kernel.Bind<ICarService>().To<CarService1>();
            _kernel.Bind<IOrderService>().To<OrderService>();
            _kernel.Bind<IShipperService>().To<ShipperService>();
        }
    }
}