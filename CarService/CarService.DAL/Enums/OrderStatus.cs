﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.DAL.Enums
{
    public enum OrderStatus
    {
        [Display(Name = "Забронирован")]
        Booked,
        [Display(Name = "Подтвержден")]
        Confirmed,
        [Display(Name = "Оплачен")]
        Payed,
        [Display(Name = "Открыт")]
        Opened,
        [Display(Name = "Закрыт")]
        Closed
    }
}
