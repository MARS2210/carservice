﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.DAL.Entities
{
    public class Car
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CarId { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public string Mark { get; set; }

        [Required]
        public string CarModel { get; set; }

        [Required]
        public string Type { get; set; }

        //[Required]
        public string Color { get; set; }
    
        public float EngineVol { get; set; }

        public decimal PricePerDay { get; set; }

        public int ReleaseYear { get; set; }

        public int MileAge { get; set; }

        public int UnitsInStock { get; set; }

        public string AdditionalInfo { get; set; }

        public byte[] ImageBytes { get; set; }

        public string ImgMimeType { get; set; }

        public int ShipperId { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual Shipper Shipper { get; set; }
    }
}
