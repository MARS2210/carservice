﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Enums;

namespace CarService.DAL.Entities
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ReturnDate { get; set; }

        public float? Discount { get; set; }

        public float? Penalty { get; set; }

        public int Quantity { get; set; }

        [Required]
        public OrderStatus PaymentState { get; set; }

        public decimal Sum { get; set; }

        public int CarId { get; set; }

        public string ApplicationUserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Car Car { get; set; }
    }
}