﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.DAL.Repository.Abstract
{

    public interface IUnitOfWork
    {
        
        ICarRepository CarRepository { get; }
        IOrderRepository OrderRepository { get; }
        IShipperRepository ShipperRepository { get; }
        IUserRepository UserRepository { get; }
        void Save();
    }
}
