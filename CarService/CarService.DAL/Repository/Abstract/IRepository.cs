﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CarService.DAL.Repository.Abstract
{
    public interface IRepository<T>

    {
        void Add(T item);
        void Edit(T item);
        void Delete(int id);
        IEnumerable<T> GetAll();
        T FindById(int id);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> sortPredicate = null, int? skip = null,
          int? take = null);
        T GetOne(Func<T, bool> filter);
    }
}
