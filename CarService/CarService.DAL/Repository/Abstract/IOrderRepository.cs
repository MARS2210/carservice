﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;

namespace CarService.DAL.Repository.Abstract
{
    public interface IOrderRepository : IRepository<Order>
    {

    }
}
