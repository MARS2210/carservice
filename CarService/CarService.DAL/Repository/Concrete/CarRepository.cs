﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.DAL.Repository.Abstract;
using CarService.Models;

namespace CarService.DAL.Repository.Concrete
{
    public class CarRepository : GenericRepository<Car>, ICarRepository
    {
        public CarRepository(ApplicationDbContext context)
            : base(context)
        {

        }
        public override IEnumerable<Car> GetAll()
        {
            return _dbSet.Include(x=>x.Shipper).ToList();
        }
    }
}
