﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Repository.Abstract;
using CarService.Models;

namespace CarService.DAL.Repository.Concrete
{
    public abstract class GenericRepository<T> : IRepository<T> where T : class
    {      
        protected readonly ApplicationDbContext Context;

        protected readonly DbSet<T> _dbSet;
    
        public GenericRepository(ApplicationDbContext context)
        {
            this.Context = context;
            this._dbSet = context.Set<T>();         
        }

        public virtual void Add(T item)
        {
            _dbSet.Add(item);         
        }

        public virtual void Edit(T item)
        {
            Context.Entry(item).State = EntityState.Modified;          
        }

        public virtual void Delete(int id)
        {
            var item = FindById(id);
            if (item == null) return;
            _dbSet.Remove(item);           
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual T FindById(int id)
        {
            return _dbSet.Find(id);
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> predicate, Func<T, object> sortPredicate,
            int? skip, int? take)
        {
            var filtered = Filtering(predicate);

            IEnumerable<T> sorted;

            if (sortPredicate != null)
            {
                sorted = filtered.OrderBy(sortPredicate);
            }
            else
            {
                sorted = filtered;
            }
            if (skip != null && take != null)
            {
                sorted = sorted.Skip((int)skip).Take((int)take);
            }
            return sorted.ToList();
        }

        private IQueryable<T> Filtering(Expression<Func<T, bool>> filter)
        {
            IQueryable<T> query = GetAll().AsQueryable();

            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public T GetOne(Func<T, bool> filter)
        {
            return _dbSet.FirstOrDefault(filter);
        }    
    }
}
