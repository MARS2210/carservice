﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.DAL.Repository.Abstract;
using CarService.Models;

namespace CarService.DAL.Repository.Concrete
{
    public class ShipperRepository : GenericRepository<Shipper>, IShipperRepository
    {
        public ShipperRepository(ApplicationDbContext context)
            : base(context)
        {

        }
    }
}
