﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.DAL.Repository.Abstract;
using CarService.Models;

namespace CarService.DAL.Repository.Concrete
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context)
            : base(context)
        {

        }

        public override IEnumerable<Order> GetAll()
        {
            return _dbSet.Include(x => x.User).Include(x=>x.Car).ToList();
        }
    }
}
