﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Repository.Abstract;
using CarService.Models;

namespace CarService.DAL.Repository.Concrete
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _context;     
        private CarRepository _carRepository;
        private OrderRepository _orderRepository;
        private ShipperRepository _shipperRepository;
        private UserRepository _userRepository;

        public ICarRepository CarRepository
        {
            get { return _carRepository ?? (_carRepository = new CarRepository(_context)); }
        }

        public IOrderRepository OrderRepository
        {
            get { return _orderRepository ?? (_orderRepository = new OrderRepository(_context)); }
        }

        public IShipperRepository ShipperRepository
        {
            get { return _shipperRepository ?? (_shipperRepository = new ShipperRepository(_context)); }
        }

        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(_context)); }
        }

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
           
        }
        public void Save()
        {
            _context.SaveChanges();           
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

