namespace CarService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCar1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cars", "Country");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "Country", c => c.String(nullable: false));
        }
    }
}
