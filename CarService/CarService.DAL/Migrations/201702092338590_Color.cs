namespace CarService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Color : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cars", "Color", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cars", "Color", c => c.String(nullable: false));
        }
    }
}
