namespace CarService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShipper : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shippers",
                c => new
                    {
                        ShipperId = c.Int(nullable: false, identity: true),
                        ShipperName = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        ContactPerson = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ShipperId);
            
            AddColumn("dbo.Cars", "Mark", c => c.String(nullable: false));
            AddColumn("dbo.Cars", "ShipperId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "DriverLicense", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "DrivingExperience", c => c.String());
            AlterColumn("dbo.Cars", "EngineVol", c => c.Single(nullable: false));
            CreateIndex("dbo.Cars", "ShipperId");
            AddForeignKey("dbo.Cars", "ShipperId", "dbo.Shippers", "ShipperId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ShipperId", "dbo.Shippers");
            DropIndex("dbo.Cars", new[] { "ShipperId" });
            AlterColumn("dbo.Cars", "EngineVol", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "DrivingExperience");
            DropColumn("dbo.AspNetUsers", "DriverLicense");
            DropColumn("dbo.Cars", "ShipperId");
            DropColumn("dbo.Cars", "Mark");
            DropTable("dbo.Shippers");
        }
    }
}
