namespace CarService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateTime2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "ReturnDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "ReturnDate", c => c.DateTime());
        }
    }
}
