namespace CarService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuantity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "Discount", c => c.Single());
            AlterColumn("dbo.Orders", "Penalty", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "Penalty", c => c.Single(nullable: false));
            AlterColumn("dbo.Orders", "Discount", c => c.Single(nullable: false));
            DropColumn("dbo.Orders", "Quantity");
        }
    }
}
