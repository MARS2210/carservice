﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.BLL.Filtration.Abstract
{
    public interface IFilter<T> where T : class
    {
        T Execute(T input);

        void Register(IFilter<T> filter);
    }
}
