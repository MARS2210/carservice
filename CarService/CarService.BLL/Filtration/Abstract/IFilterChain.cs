﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CarService.BLL.Filtration.Abstract
{
    public interface IFilterChain<T>
           where T : Expression
    {
        IFilter<T> Root { get; }

        T Execute(T input);

        IFilterChain<T> Register(IFilter<T> filter);
    }
}
