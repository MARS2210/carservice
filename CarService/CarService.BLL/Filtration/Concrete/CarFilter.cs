﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.BLL.Infrastructure;

namespace CarService.BLL.Filtration.Concrete
{
    public class CarFilter : BaseFilter<Expression<Func<Car, bool>>>
    {
        private List<string> Marks { get; set; }

        public CarFilter(List<string> marks)
        {
            this.Marks = marks;
        }

        protected override Expression<Func<Car, bool>> Process(Expression<Func<Car, bool>> input)
        {
            if (Marks.Count() != 0)
            {
                return input.And(x => x.Mark != null ? Marks.Contains(x.Mark) : x.Mark == "");
            }

            return input;

        }
    }
}