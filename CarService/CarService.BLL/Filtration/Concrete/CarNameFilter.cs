﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.BLL.Infrastructure;

namespace CarService.BLL.Filtration.Concrete
{
    public class CarNameFilter : BaseFilter<Expression<Func<Car, bool>>>
    {
        private readonly string searchString;

        public CarNameFilter(string searchString)
        {
            this.searchString = searchString;
        }

        protected override Expression<Func<Car, bool>> Process(Expression<Func<Car, bool>> input)
        {         
            return string.IsNullOrEmpty(searchString)
                       ? input
                        : input.And(x =>  x.CarModel.ToLower().Contains(searchString.ToLower()));
        }
    }
}
