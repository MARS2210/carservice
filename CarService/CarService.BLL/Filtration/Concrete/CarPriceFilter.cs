﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.BLL.Infrastructure;

namespace CarService.BLL.Filtration.Concrete
{
    public class CarPriceFilter : BaseFilter<Expression<Func<Car, bool>>>
    {
        private readonly decimal? maxPrice;

        private readonly decimal? minPrice;

        public CarPriceFilter(decimal? minPrice, decimal? maxPrice)
        {
            this.minPrice = minPrice;
            this.maxPrice = maxPrice;
        }

        protected override Expression<Func<Car, bool>> Process(Expression<Func<Car, bool>> input)
        {
            if (minPrice != null && maxPrice != null)
            {
                input = input.And(x => x.PricePerDay >= minPrice && x.PricePerDay <= maxPrice);
            }
            else if (minPrice != null)
            {
                input = input.And(x => x.PricePerDay >= minPrice);
            }
            else if (maxPrice != null)
            {
                input = input.And(x => x.PricePerDay <= maxPrice);
            }

            return input;
        }
    }
}
