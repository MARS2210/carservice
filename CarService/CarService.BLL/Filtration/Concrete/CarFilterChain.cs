﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CarService.BLL.Filtration.Abstract;
using CarService.DAL.Entities;

namespace CarService.BLL.Filtration.Concrete
{
    public class CarFilterChain : IFilterChain<Expression<Func<Car, bool>>>
    {
        public IFilter<Expression<Func<Car, bool>>> Root { get; private set; }

        public Expression<Func<Car, bool>> Execute(Expression<Func<Car, bool>> input)
        {
            return Root.Execute(input);
        }

        public IFilterChain<Expression<Func<Car, bool>>> Register(IFilter<Expression<Func<Car, bool>>> filter)
        {
            if (Root == null)
            {
                Root = filter;
            }
            else
            {
                Root.Register(filter);
            }

            return this;
        }
    }
}
