﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.Filtration.Concrete;
using CarService.BLL.Infrastructure;
using CarService.BLL.Interfaces;
using CarService.DAL.Entities;
using CarService.DAL.Repository.Abstract;
using CarService.DAL.Enums;

namespace CarService.BLL.Services
{
    public class CarService1 : ICarService
    {
        public CarService1(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public IEnumerable<CarDto> GetAllCars()
        {
            var cars = Database.CarRepository.GetAll();
            return Mapper.Map<IEnumerable<Car>, IEnumerable<CarDto>>(cars);
        }

        public void AddGame(CarDto carDto)
        {
            if (carDto == null)
            {
                throw new ArgumentNullException();
            }
            if (carDto == null)
            {
                throw new ArgumentNullException();
            }
            var carObj = Mapper.Map<CarDto, Car>(carDto);

            Database.CarRepository.Add(carObj);
            Database.Save();

        }

        public CarDto GetCarById(int id)
        {
            var car = Database.CarRepository.FindById(id);
            var carDto = Mapper.Map<Car, CarDto>(car);
            return carDto;
        }


        public void EditCar(CarDto carDto)
        {
            if (carDto == null)
            {
                throw new ArgumentNullException();
            }
            var carObj = Mapper.Map<CarDto, Car>(carDto);
            Database.CarRepository.Edit(carObj);
            Database.Save();
        }

        public void Remove(int id)
        {
            Database.CarRepository.Delete(id);
            Database.Save();
        }

        public int CheckUnitsInStock(int id)
        {
            var unitsInStock = Database.CarRepository.FindById(id).UnitsInStock;
            return unitsInStock;
        }


        public IEnumerable<CarDto> GetFiltered(FilterDto filters)
        {
            var gameFilterChain = new CarFilterChain();
            RegisterFilters(gameFilterChain, filters);
            var predicate = gameFilterChain.Execute(x => x.Number != "");
            return Mapper.Map<IEnumerable<CarDto>>(Database.CarRepository.Get(predicate));
        }

        private void RegisterFilters(CarFilterChain gameFilterChain, FilterDto filters)
        {
            if (filters.Marks.Count(x => x.Checked) != 0)
            {
                var list = new List<string>();
                foreach (var item in filters.Marks.Where(x => x.Checked))
                {
                    list.Add(item.Name);
                }
                gameFilterChain.Register(new CarFilter(list));
            }

            gameFilterChain.Register(new CarNameFilter(filters.SearchString));
            gameFilterChain.Register(new CarPriceFilter(filters.MinPrice, filters.MaxPrice));
        }


        public Tuple<byte[], string> GetImage(int id)
        {
            var game = Database.CarRepository.FindById(id);
            if (game == null || game.ImgMimeType == null)
            {
                byte[] imgArray = Convert.FromBase64String(Constants.DefaultPicture);
                return new Tuple<byte[], string>(imgArray, "image/jpeg");
            }
            return new Tuple<byte[], string>(game.ImageBytes, game.ImgMimeType);
        }

        public void DownloadImage(CarDto car, string contentType, int contentLength)
        {
            car.ImgMimeType = contentType;
            car.ImageBytes = new byte[contentLength];
        }

        public bool Check(string idUser, int idCar)
        {

            var user = Database.UserRepository.GetOne(x=> x.Id ==idUser);
           
                var orderUser = user.Orders.Where(x => x.PaymentState != OrderStatus.Closed);
                foreach (var o in orderUser)
                {
                    if (o.CarId == idCar)
                    {
                        return true;
                       
                    }
                }
                return false;   
        }

        public bool CheckLicense(string idUser)
        {

            var user = Database.UserRepository.GetOne(x => x.Id == idUser);

            var license = user.DriverLicense;
          
                if (license)
                {
                    return true;

                }
            
            return false;
        }
    }
}
