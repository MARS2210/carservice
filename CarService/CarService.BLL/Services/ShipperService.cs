﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.Interfaces;
using CarService.DAL.Entities;
using CarService.DAL.Repository.Abstract;

namespace CarService.BLL.Services
{
    public  class ShipperService: IShipperService
    {
        public ShipperService(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public IEnumerable<ShipperDto> GetAllShippers()
        {
            var shippers = Database.ShipperRepository.GetAll();
            return Mapper.Map<IEnumerable<Shipper>, IEnumerable<ShipperDto>>(shippers);
        }

        public void AddShipper(ShipperDto shipperDto)
        {
            if (shipperDto == null)
            {
                throw new ArgumentNullException();
            }
            if (shipperDto == null)
            {
                throw new ArgumentNullException();
            }
            var shipperObj = Mapper.Map<ShipperDto, Shipper>(shipperDto);

            Database.ShipperRepository.Add(shipperObj);
            Database.Save();

        }

        public ShipperDto GetShipperById(int id)
        {
            var shipper = Database.ShipperRepository.FindById(id);
            var shipperDto = Mapper.Map<Shipper, ShipperDto>(shipper);
            return shipperDto;
        }


        public void EditShipper(ShipperDto shipperDto)
        {
            if (shipperDto == null)
            {
                throw new ArgumentNullException();
            }
            var shipperObj = Mapper.Map<ShipperDto, Shipper>(shipperDto);
            Database.ShipperRepository.Edit(shipperObj);
            Database.Save();
        }

        public void Remove(int id)
        {
            Database.ShipperRepository.Delete(id);
            Database.Save();
        }
    }
}
