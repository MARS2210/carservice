﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.BLL.HelpModels;
using CarService.BLL.Interfaces;
using CarService.DAL.Entities;
using CarService.DAL.Enums;
using CarService.DAL.Repository.Abstract;

namespace CarService.BLL.Services
{
    public class OrderService : IOrderService
    {
        public OrderService(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public IEnumerable<OrderDto> GetAllOrders()
        {
            var orders = Database.OrderRepository.GetAll();
            return Mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
        }
        public IEnumerable<GroupDate> GrouByDate()
        {
            var orders = Database.OrderRepository.GetAll().AsEnumerable().GroupBy(x => x.OrderDate.ToMonthName()).ToList();
            var i = orders.Select(x=> new GroupDate()
            {
                Date = x.Key,
                Cost = x.Sum(y => y.Sum),
                Count = x.Sum(y => y.Quantity)

            }).ToList();         
            return i;
        }
        public IEnumerable<OrderDto> GetOrdersBetweenDates(DateTime start, DateTime end)
        {
            var orders = Database.OrderRepository.Get(x =>  x.OrderDate > start && x.OrderDate < end);
            var ordersDto = Mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
            return ordersDto;
        }
        public IEnumerable<OrderDto> GetOrdersBetweenYears(int start)
        {
            var orders = Database.OrderRepository.Get(x => x.OrderDate.Year == start);
            var ordersDto = Mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
            return ordersDto;
        }
        public IEnumerable<OrderDto> GetAllOrdersForUser(string id)
        {
            var orders = Database.OrderRepository.Get(x=>x.User.Id==id && x.PaymentState!=OrderStatus.Closed);
            return Mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
        }

        public void AddOrder(OrderDto orderDto)
        {
            if (orderDto == null)
            {
                throw new ArgumentNullException();
            }
            if (orderDto == null)
            {
                throw new ArgumentNullException();
            }
            var orderObj = Mapper.Map<OrderDto, Order>(orderDto);
            var car = Database.CarRepository.FindById(orderDto.CarId);
            var divTime = orderDto.EndDate.Day - orderDto.StartDate.Day;
            if (orderDto.Discount.HasValue)
            {
                orderObj.Sum = car.PricePerDay*divTime*orderDto.Quantity*((100 - (decimal) orderDto.Discount)/100);
            }
            else
            {
                orderObj.Sum = car.PricePerDay*divTime*orderDto.Quantity;
            }
            orderObj.OrderDate = DateTime.UtcNow;
            orderObj.PaymentState = OrderStatus.Booked;
            Database.OrderRepository.Add(orderObj);
            car.UnitsInStock -= orderDto.Quantity;
            Database.CarRepository.Edit(car);
            Database.Save();
        }

        public OrderDto GetOrderById(int id)
        {
            var order = Database.OrderRepository.FindById(id);
            var orderDto = Mapper.Map<Order, OrderDto>(order);
            return orderDto;
        }

        private IEnumerable<Order> GetOrderByUserId(string id)
        {
            var orders= Database.OrderRepository.Get(x=>x.ApplicationUserId==id);
           
            return orders;
        }

        public int CountWithPenalty(string id)
        {
            var order = GetOrderByUserId(id);
            var count = order.Where(x => x.Penalty != null).Count();
            return count;
        }

        public int CountWithoutPenalty(string id)
        {
            var order = GetOrderByUserId(id);
            var count = order.Where(x => x.Penalty == null).Count();
            return count;
        }

        public void EditOrder(OrderDto orderDto, int? oldQuantity)
        {
            if (orderDto == null)
            {
                throw new ArgumentNullException();
            }
           var orderObj = Database.OrderRepository.FindById(orderDto.OrderId);

            Mapper.Map(orderDto, orderObj);
                   
            var car = Database.CarRepository.FindById(orderObj.CarId);
                    
            var divTime = orderObj.EndDate.Day - orderObj.StartDate.Day;
            if (orderDto.Discount.HasValue)
            {
                orderObj.Sum = car.PricePerDay * divTime * orderDto.Quantity * ((100 - (decimal)orderDto.Discount) / 100);
            }
            if (orderDto.Penalty.HasValue )
            {
                orderObj.Sum = car.PricePerDay * divTime * orderDto.Quantity * ((100 + (decimal)orderDto.Penalty) / 100);
            }
            if (!orderDto.Discount.HasValue && !orderDto.Penalty.HasValue)
            {
                orderObj.Sum = car.PricePerDay * divTime * orderDto.Quantity;
            }
            if (orderObj.PaymentState == OrderStatus.Closed)
            {
                orderObj.ReturnDate = DateTime.UtcNow;
            }
            else
            {
                orderObj.ReturnDate = null;
            }
            Database.OrderRepository.Edit(orderObj);
            var temp = (car.UnitsInStock + (int)oldQuantity - orderDto.Quantity);
            car.UnitsInStock = temp;
            Database.CarRepository.Edit(car);
            Database.Save();         
        }

        public void Remove(int id)
        {
            var order = Database.OrderRepository.FindById(id);
            var quantity = order.Quantity;
            Database.OrderRepository.Delete(id);
            var car = Database.CarRepository.FindById(order.CarId);
            car.UnitsInStock += quantity;
            Database.CarRepository.Edit(car);
            Database.Save();
        }

        public void ChangeOrderToConfirmed( int id )
        {
            var order = Database.OrderRepository.FindById(id);
            order.PaymentState = OrderStatus.Confirmed;
            Database.OrderRepository.Edit(order);
            Database.Save();
        }

  
    }
}
