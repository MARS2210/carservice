﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.HelpModels
{
    public class CheckModel
    {
        public string Name
        {
            get;
            set;
        }
        public bool Checked
        {
            get;
            set;
        }
    }
}