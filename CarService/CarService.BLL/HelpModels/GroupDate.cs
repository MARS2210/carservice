﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.BLL.HelpModels
{
    public class GroupDate
    {
        public string Date { get; set; }
        public int Count { get; set; }
        public Decimal Cost { get; set; }
    }
}
