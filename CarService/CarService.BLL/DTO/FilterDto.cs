﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.HelpModels;

namespace CarService.BLL.DTO
{
    public class FilterDto
    {
        
        public List<CheckModel> Marks { get; set; }

        public string[] MarkList { get; set; }
  
        public string SearchString { get; set; }

        public decimal? MinPrice { get; set; }

        public decimal? MaxPrice { get; set; }
    }
}
