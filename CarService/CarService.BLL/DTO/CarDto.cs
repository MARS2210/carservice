﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.BLL.DTO
{
    public class CarDto
    {
        public int CarId { get; set; }
      
        public string Number { get; set; }
    
        public string Mark { get; set; }
        
        public string CarModel { get; set; }
        
        public string Type { get; set; }
     
        public string Color { get; set; }

        public float EngineVol { get; set; }

        public decimal PricePerDay { get; set; }

        public int ReleaseYear { get; set; }

        public int MileAge { get; set; }

        public int UnitsInStock { get; set; }

        public string AdditionalInfo { get; set; }

        public byte[] ImageBytes { get; set; }

        public string ImgMimeType { get; set; }

        public int ShipperId { get; set; }
        
        public ShipperDto Shipper { get; set; }
    }
}
