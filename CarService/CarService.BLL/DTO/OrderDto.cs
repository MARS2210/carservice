﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.DAL.Entities;
using CarService.DAL.Enums;

namespace CarService.BLL.DTO
{
    public class OrderDto
    {     
        public int OrderId { get; set; }
     
        public DateTime OrderDate { get; set; }
      
        public DateTime StartDate { get; set; }
      
        public DateTime EndDate { get; set; }

        public DateTime? ReturnDate { get; set; }

        public float? Discount { get; set; }

        public float? Penalty { get; set; }

        public int Quantity { get; set; }
     
        public OrderStatus PaymentState { get; set; }

        public decimal Sum { get; set; }

        public int CarId { get; set; }

        public string ApplicationUserId { get; set; }

        public ApplicationUser User { get; set; }

        public CarDto Car { get; set; }
    }
}
