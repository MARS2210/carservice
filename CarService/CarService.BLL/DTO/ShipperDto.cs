﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarService.BLL.DTO
{
    public class ShipperDto
    {              
        public int ShipperId { get; set; }
     
        public string ShipperName { get; set; }
       
        public string Country { get; set; }

        public string Address { get; set; }

        public string ContactPerson { get; set; }
      
        public string Phone { get; set; }

        //public ICollection<CarDto> Cars { get; set; }
    }
}
