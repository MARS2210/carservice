﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CarService.BLL.DTO;
using CarService.DAL.Entities;

namespace CarService.BLL.Infrastructure
{
    public class DaltoDtoModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToDtoMappingProfile"; }
        }

        protected override void Configure()
        {           
            CreateMap<Car, CarDto>().ReverseMap();

            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>()           
            .ForSourceMember(x => x.Car, opt => opt.Ignore());

            CreateMap<Shipper, ShipperDto>().ReverseMap();
        }
    }
}

