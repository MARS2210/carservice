﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.BLL.DTO;
using CarService.BLL.HelpModels;
using CarService.DAL.Entities;

namespace CarService.BLL.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderDto> GetAllOrders();
        void AddOrder(OrderDto orderDto);
        OrderDto GetOrderById(int id);
        void EditOrder(OrderDto orderDto, int? oldQuantity);
        void Remove(int id);
        int CountWithPenalty(string id);
        int CountWithoutPenalty(string id);
        void ChangeOrderToConfirmed(int id);
        IEnumerable<OrderDto> GetAllOrdersForUser(string id);
        IEnumerable<GroupDate> GrouByDate();
        IEnumerable<OrderDto> GetOrdersBetweenDates(DateTime start, DateTime end);
        IEnumerable<OrderDto> GetOrdersBetweenYears(int start);
    }
}
