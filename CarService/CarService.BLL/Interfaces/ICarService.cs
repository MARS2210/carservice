﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.BLL.DTO;

namespace CarService.BLL.Interfaces
{
    public interface ICarService
    {
        IEnumerable<CarDto> GetAllCars();
        void AddGame(CarDto carDto);
        CarDto GetCarById(int id);
        void EditCar(CarDto carDto);
        void Remove(int id);
        int CheckUnitsInStock(int id);
        IEnumerable<CarDto> GetFiltered(FilterDto filters);
        Tuple<byte[], string> GetImage(int id);
        void DownloadImage(CarDto game, string contentType, int contentLength);
        bool Check(string idUser, int idCar);
        bool CheckLicense(string idUser);
    }
}
