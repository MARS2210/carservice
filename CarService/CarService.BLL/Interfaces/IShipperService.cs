﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarService.BLL.DTO;

namespace CarService.BLL.Interfaces
{
    public interface IShipperService
    {
        IEnumerable<ShipperDto> GetAllShippers();
        void AddShipper(ShipperDto shipperDto);
        ShipperDto GetShipperById(int id);
        void EditShipper(ShipperDto shipperDto);
        void Remove(int id);
    }
}
